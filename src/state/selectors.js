import { createSelector } from 'reselect';
import { FAILURE_STATUS, REQUEST_STATUS, SUCCESS_STATUS } from '../helpers';

export const getValueForSectionA = state => state.form.a;
export const getValueForSectionB = state => state.form.b;
export const getValueForSectionC = state => state.form.c;
export const getValueForSectionT = state => state.form.text;

export const getErrorForSectionT = state => state.error.text;
export const getErrorForMainForm = state => state.error.form;

const getStatusForSectionT = state => state.status.text;
const getStatusForMainForm = state => state.status.form;

export const getLoadingStatusForSectionT = createSelector(
  [getStatusForSectionT],
  status => status === REQUEST_STATUS
);

export const getValidStatusForSectionT = createSelector(
  [getStatusForSectionT],
  status => status === SUCCESS_STATUS
);

export const getInvalidStatusForSectionT = createSelector(
  [getStatusForSectionT],
  status => status === FAILURE_STATUS
);

export const getLoadingStatusForMainForm = createSelector(
  [getStatusForMainForm],
  status => status === REQUEST_STATUS
);

export const getValidStatusForMainForm = createSelector(
  [getStatusForMainForm],
  status => status === SUCCESS_STATUS
);

export const getInvalidStatusForMainForm = createSelector(
  [getStatusForMainForm],
  status => status === FAILURE_STATUS
);

export const getDisabledStatusForSectionB = createSelector(
  [getValueForSectionA],
  value => value.length === 0
);

export const getDisabledStatusForSectionT = createSelector(
  [getDisabledStatusForSectionB, getValueForSectionB, getLoadingStatusForSectionT],
  (isSectionBDisabled, sectionBValue, isSectionTextLoading) =>
    isSectionBDisabled || !sectionBValue || isSectionTextLoading
);

export const getDisabledStatusForSectionC = createSelector(
  [getDisabledStatusForSectionT, getValidStatusForSectionT],
  (isSectionTextDisabled, isSectionTextValid) => isSectionTextDisabled || !isSectionTextValid
);

export const getDisabledStatusForMainForm = createSelector(
  [getDisabledStatusForSectionC, getLoadingStatusForMainForm, getValueForSectionC],
  (isSectionCDisabled, isMainFormLoading, sectionCValue) => isSectionCDisabled || isMainFormLoading || !sectionCValue
);
