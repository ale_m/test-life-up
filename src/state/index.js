import { combineReducers } from 'redux';
import {
  CHANGE_SECTION_A,
  CHANGE_SECTION_B,
  CHANGE_SECTION_C,
  CHANGE_SECTION_T,
  CHECK_TEXT__REQUEST,
  CHECK_TEXT__SUCCESS,
  CHECK_TEXT__FAILURE,
  SEND_FORM__REQUEST,
  SEND_FORM__FAILURE,
  SEND_FORM__SUCCESS
} from './actions';
import { FAILURE_STATUS, REQUEST_STATUS, SUCCESS_STATUS } from '../helpers';

const INITIAL_STATE_FORM = {
  a: [],
  b: '',
  c: '',
  text: ''
};

const formReducer = (state = INITIAL_STATE_FORM, { type, payload = {} }) => {
  switch (type) {
    case CHANGE_SECTION_A:
      const { label, isChecked } = payload;
      const a = isChecked ? [...state.a, label] : state.a.filter(field => field !== label);

      return { ...state, a };
    case CHANGE_SECTION_B:
      return { ...state, b: payload.value };
    case CHANGE_SECTION_C:
      return { ...state, c: payload.value };
    case CHANGE_SECTION_T:
      return { ...state, text: payload.value };
    default:
      return state;
  }
};

const INITIAL_STATE_ERROR = {
  text: '',
  form: ''
};

const errorReducer = (state = INITIAL_STATE_ERROR, { type, payload = {} }) => {
  switch (type) {
    case CHANGE_SECTION_T:
    case SEND_FORM__SUCCESS:
    case CHECK_TEXT__SUCCESS:
      return { ...INITIAL_STATE_ERROR };
    case CHECK_TEXT__FAILURE:
      return { ...state, text: payload.error };
    case SEND_FORM__FAILURE:
      return { ...state, form: payload.error };
    default:
      return state;
  }
};

const INITIAL_STATE_STATUSES = {
  text: '',
  form: ''
};

const statusReducer = (state = INITIAL_STATE_STATUSES, { type, payload = {} }) => {
  switch (type) {
    case CHECK_TEXT__REQUEST:
      return { ...state, text: REQUEST_STATUS, form: '' };
    case CHECK_TEXT__SUCCESS:
      return { ...state, text: SUCCESS_STATUS };
    case CHECK_TEXT__FAILURE:
      return { ...state, text: FAILURE_STATUS };
    case CHANGE_SECTION_T:
      return { ...state, text: '', form: '' };
    case SEND_FORM__REQUEST:
      return { ...state, form: REQUEST_STATUS };
    case SEND_FORM__SUCCESS:
      return { ...state, form: SUCCESS_STATUS };
    case SEND_FORM__FAILURE:
      return { ...state, form: FAILURE_STATUS };
    case CHANGE_SECTION_A:
    case CHANGE_SECTION_B:
    case CHANGE_SECTION_C:
      return { ...state, form: '' };
    default:
      return state;
  }
};

export default combineReducers({ form: formReducer, error: errorReducer, status: statusReducer });
