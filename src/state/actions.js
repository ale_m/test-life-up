import * as api from '../api';

export const CHANGE_SECTION_A = 'CHANGE_SECTION_A';
export const CHANGE_SECTION_B = 'CHANGE_SECTION_B';
export const CHANGE_SECTION_T = 'CHANGE_SECTION_T';
export const CHANGE_SECTION_C = 'CHANGE_SECTION_C';

export const SEND_FORM__REQUEST = 'SEND_FORM__REQUEST';
export const SEND_FORM__SUCCESS = 'SEND_FORM__SUCCESS';
export const SEND_FORM__FAILURE = 'SEND_FORM__FAILURE';

export const CHECK_TEXT__REQUEST = 'CHECK_TEXT__REQUEST';
export const CHECK_TEXT__SUCCESS = 'CHECK_TEXT__SUCCESS';
export const CHECK_TEXT__FAILURE = 'CHECK_TEXT__FAILURE';

export function changeSectionA(label, isChecked) {
  return {
    type: CHANGE_SECTION_A,
    payload: {
      label,
      isChecked
    }
  };
}

export function changeSectionB(value) {
  return {
    type: CHANGE_SECTION_B,
    payload: {
      value
    }
  };
}

export function changeSectionT(value) {
  return {
    type: CHANGE_SECTION_T,
    payload: {
      value
    }
  };
}

export function changeSectionC(value) {
  return {
    type: CHANGE_SECTION_C,
    payload: {
      value
    }
  };
}

export function checkTextRequest() {
  return {
    type: CHECK_TEXT__REQUEST
  };
}

export function checkTextSuccess(value) {
  return {
    type: CHECK_TEXT__SUCCESS,
    payload: {
      value
    }
  };
}

export function checkTextFailure(error) {
  return {
    type: CHECK_TEXT__FAILURE,
    payload: {
      error
    }
  };
}

export function sendFormRequest() {
  return {
    type: SEND_FORM__REQUEST
  };
}

export function sendFormSuccess(value) {
  return {
    type: SEND_FORM__SUCCESS,
    payload: {
      value
    }
  };
}

export function sendFormFailure(error) {
  return {
    type: SEND_FORM__FAILURE,
    payload: {
      error
    }
  };
}

export function checkText() {
  return (dispatch, getState) => {
    const { text } = getState().form;

    dispatch(checkTextRequest());

    api
      .checkTextField(text)
      .then(response => dispatch(checkTextSuccess(response)))
      .catch(error => dispatch(checkTextFailure(error.message)));
  };
}

export function submitForm() {
  return (dispatch, getState) => {
    const { form } = getState();

    dispatch(sendFormRequest());

    api
      .submit(form)
      .then(response => dispatch(sendFormSuccess(response)))
      .catch(error => dispatch(sendFormFailure(error.message)));
  };
}
