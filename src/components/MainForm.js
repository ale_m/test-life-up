import React from 'react';
import { connect } from 'react-redux';
import { Alert, Button, Form } from 'reactstrap';
import SectionA from './SectionA';
import SectionB from './SectionB';
import SectionC from './SectionC';
import SectionT from './SectionT';
import PropTypes from 'prop-types';
import {
  getErrorForMainForm,
  getValidStatusForMainForm,
  getInvalidStatusForMainForm,
  getDisabledStatusForMainForm
} from '../state/selectors';
import { submitForm } from '../state/actions';

const MainForm = ({ error, isValid, isDisabled, isInvalid, onSubmit }) => {
  const handleSubmit = e => {
    e.preventDefault();

    onSubmit();
  };

  return (
    <React.Fragment>
      <Form className="form bg-white shadow rounded border w-100 p-3 mb-3" onSubmit={handleSubmit}>
        <SectionA />
        <SectionB />
        <SectionT />
        <SectionC />
        <h3>Step 5</h3>
        <Button color="primary" type="submit" disabled={isDisabled}>
          Submit
        </Button>
      </Form>
      {isInvalid ? <Alert color="danger">{error}</Alert> : null}
      {isValid ? <Alert color="success">Success!</Alert> : null}
    </React.Fragment>
  );
};

MainForm.defaultProps = {
  error: '',
  isValid: false,
  isInvalid: false,
  isDisabled: true
};

MainForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  isDisabled: PropTypes.bool,
  isValid: PropTypes.bool,
  isInvalid: PropTypes.bool
};

const mapStateToProps = state => ({
  error: getErrorForMainForm(state),
  isDisabled: getDisabledStatusForMainForm(state),
  isValid: getValidStatusForMainForm(state),
  isInvalid: getInvalidStatusForMainForm(state)
});

const mapDispatchToProps = {
  onSubmit: submitForm
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainForm);
