import React from 'react';
import { connect } from 'react-redux';
import { ButtonGroup, Button } from 'reactstrap';
import PropTypes from 'prop-types';
import { changeSectionB } from '../state/actions';
import { getValueForSectionB, getDisabledStatusForSectionB } from '../state/selectors';

const FIELDS = ['B1', 'B2'];

const SectionB = ({ onChange, value, isDisabled }) => {
  const handleChange = e => {
    const { name } = e.target;

    onChange(name);
  };

  const isActive = field => value === field;

  return (
    <section className="section-b mb-4">
      <h3>Step 2</h3>
      <ButtonGroup>
        {FIELDS.map(field => (
          <Button
            color="primary"
            name={field}
            onClick={handleChange}
            active={isActive(field)}
            key={field}
            disabled={isDisabled}
          >
            {field}
          </Button>
        ))}
      </ButtonGroup>
    </section>
  );
};

SectionB.defaultProps = {
  value: '',
  isDisabled: true
};

SectionB.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  isDisabled: PropTypes.bool
};

const mapStateToProps = state => ({
  value: getValueForSectionB(state),
  isDisabled: getDisabledStatusForSectionB(state)
});

const mapDispatchToProps = {
  onChange: changeSectionB
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionB);
