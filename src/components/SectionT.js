import React from 'react';
import { connect } from 'react-redux';
import { Button, FormGroup, FormFeedback, Label, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import PropTypes from 'prop-types';
import { changeSectionT, checkText } from '../state/actions';
import {
  getValueForSectionT,
  getErrorForSectionT,
  getInvalidStatusForSectionT,
  getValidStatusForSectionT,
  getDisabledStatusForSectionT
} from '../state/selectors';

const SectionT = ({ onChange, onCheck, value, error, isValid, isInvalid, isDisabled }) => {
  const handleChange = e => {
    const { value } = e.target;

    onChange(value);
  };

  return (
    <section className="section-t mb-4">
      <h3>Step 3</h3>

      <FormGroup>
        <Label for="text">Awesome label</Label>
        <InputGroup>
          <Input
            type="text"
            name="text"
            id="text"
            onChange={handleChange}
            value={value}
            valid={isValid}
            invalid={isInvalid}
            disabled={isDisabled}
          />
          <InputGroupAddon addonType="append">
            <Button color="primary" onClick={onCheck} disabled={isDisabled || !value}>
              Check
            </Button>
          </InputGroupAddon>
        </InputGroup>
        {isInvalid ? <FormFeedback className="d-block">{error}</FormFeedback> : null}
      </FormGroup>
    </section>
  );
};

SectionT.defaultProps = {
  value: '',
  error: '',
  isValid: false,
  isInvalid: false,
  isDisabled: true
};

SectionT.propTypes = {
  onChange: PropTypes.func.isRequired,
  onCheck: PropTypes.func.isRequired,
  value: PropTypes.string,
  error: PropTypes.string,
  isValid: PropTypes.bool,
  isInvalid: PropTypes.bool,
  isDisabled: PropTypes.bool
};

const mapStateToProps = state => ({
  value: getValueForSectionT(state),
  error: getErrorForSectionT(state),
  isValid: getValidStatusForSectionT(state),
  isInvalid: getInvalidStatusForSectionT(state),
  isDisabled: getDisabledStatusForSectionT(state)
});

const mapDispatchToProps = {
  onChange: changeSectionT,
  onCheck: checkText
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionT);
