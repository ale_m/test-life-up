import React from 'react';
import { connect } from 'react-redux';
import { FormGroup, Label, Input } from 'reactstrap';
import PropTypes from 'prop-types';
import { changeSectionC } from '../state/actions';
import { getDisabledStatusForSectionC, getValueForSectionC } from '../state/selectors';

const OPTIONS = ['C1', 'C2', 'C3'];

const SectionB = ({ value, onChange, isDisabled }) => {
  const handleChange = e => {
    const { value } = e.target;

    onChange(value);
  };

  return (
    <section className="section-c mb-4">
      <h3>Step 4</h3>
      <FormGroup>
        <Label for="something">Select</Label>
        <Input type="select" name="select" id="something" onChange={handleChange} value={value} disabled={isDisabled}>
          {OPTIONS.map(option => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </Input>
      </FormGroup>
    </section>
  );
};

SectionB.defaultProps = {
  value: '',
  isDisabled: true
};

SectionB.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  isDisabled: PropTypes.bool
};

const mapStateToProps = state => ({
  value: getValueForSectionC(state),
  isDisabled: getDisabledStatusForSectionC(state)
});

const mapDispatchToProps = {
  onChange: changeSectionC
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionB);
