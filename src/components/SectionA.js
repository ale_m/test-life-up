import React from 'react';
import { connect } from 'react-redux';
import { FormGroup, Label, Input } from 'reactstrap';
import PropTypes from 'prop-types';
import { changeSectionA } from '../state/actions';
import { getValueForSectionA } from '../state/selectors';

const FIELDS = ['A1', 'A2'];

const SectionA = ({ onChange, values }) => {
  const handleChange = e => {
    const { name, checked } = e.target;

    onChange(name, checked);
  };

  const isChecked = field => values.includes(field);

  return (
    <section className="section-a mb-4">
      <h3>Step 1</h3>
      {FIELDS.map(field => (
        <FormGroup key={field} check inline>
          <Label check>
            <Input type="checkbox" name={field} onChange={handleChange} checked={isChecked(field)} /> {field}
          </Label>
        </FormGroup>
      ))}
    </section>
  );
};

SectionA.defaultProps = {
  values: []
};

SectionA.propTypes = {
  values: PropTypes.array,
  onChange: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  values: getValueForSectionA(state)
});

const mapDispatchToProps = {
  onChange: changeSectionA
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionA);
