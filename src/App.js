import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import MainForm from './components/MainForm';

const App = () => {
  return (
    <Container className="app p-4">
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <h1>Please fill out the form</h1>
          <MainForm />
        </Col>
      </Row>
    </Container>
  );
};

export default App;
